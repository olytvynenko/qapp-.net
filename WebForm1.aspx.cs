﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;

namespace test_WebApplication
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string formString = Request.Form["str"];

            Label1.Text = "String: " + formString;

            try
            {
                using (TicketService.eID ts = new TicketService.eID())
                {
                    TicketService.ToBeSigned signed = new TicketService.ToBeSigned();
                    TicketService.Assertion assertion;
                    TicketService.DistinguishedName dn;

                    ts.AuthenticationAddress = "https://ticket.siriusit.net";
                    ts.ResolverAddress = "https://ticket-resolver2.siriusit.net:443";
                    ts.System = "qapp";

                    signed.WriteSignText(formString);
                    signed.WriteDisplayContent(formString);
                    
                    assertion = ts.Sign(signed);

                    HttpContext.Current.Response.Write('\n' + assertion.IsValid.ToString());

                    HttpContext.Current.Response.Write('\n' + assertion.ToString());

                    if (assertion.IsValid)
                    {
                        dn = assertion.Subject;

                        HttpContext.Current.Response.Write(dn.ToString());
                        HttpContext.Current.Response.Write(dn.Get(TicketService.
                            DistinguishedName.SerialNumber));
                        HttpContext.Current.Response.Write("<br/>SignUrl = " +
                                                           assertion.GetAttributeValue("SignUrl", ""));
                        HttpContext.Current.Response.Write("<br/>SignDigest = " +
                                                           assertion.GetAttributeValue("SignDigest", ""));
                        HttpContext.Current.Response.Write("<br/>SignedTextLength = " +
                                                           assertion.GetAttributeValue("SignTextLength", ""));
                        HttpContext.Current.Response.Write("<br/>DisplayContentLength = " +
                                                           assertion.GetAttributeValue("DisplayContentLength", ""));
                        HttpContext.Current.Response.Write("<br/>Certificate = " +
                                                           assertion.GetAttributeValue("Certificate", ""));
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("<br/>Null assertion");
                    }

                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                HttpContext.Current.Response.Write("<br/>TS thread abort exception");
                throw; // throw further (response.End() was called)
            }
            catch (TicketService.CancelException exception)
            {
                HttpContext.Current.Response.Write("<br/>TS cancel exception");
                HttpContext.Current.Response.Write("<br/>The request was canceled!");
                HttpContext.Current.Response.Write("<br/>Message: " + exception.Message);
                HttpContext.Current.Response.Write("<br/>Source: " + exception.Source);
                HttpContext.Current.Response.Write("<br/>Target site: " + exception.TargetSite);
                HttpContext.Current.Response.Write("<br/>Stack trace: " + exception.StackTrace);
            }
            catch (System.Exception exception)
            {
                HttpContext.Current.Response.Write("System exception");
                if (TicketService.eID.IsCancelException(exception))
                { // note: this is not reached if catching the CancelException above
                    HttpContext.Current.Response.Write("<br/>The request was canceled!");
                    HttpContext.Current.Response.Write("<br/>Message: " + exception.Message);
                    HttpContext.Current.Response.Write("<br/>Source: " + exception.Source);
                    HttpContext.Current.Response.Write("<br/>Target site: " + exception.TargetSite);
                    HttpContext.Current.Response.Write("<br/>Stack trace: " + exception.StackTrace);
                }
                else
                {
                    HttpContext.Current.Response.Write("<br/>Other system exception");
                    HttpContext.Current.Response.Write("<br/>The request was canceled!");
                    HttpContext.Current.Response.Write("<br/>Message: " + exception.Message);
                    HttpContext.Current.Response.Write("<br/>Source: " + exception.Source);
                    HttpContext.Current.Response.Write("<br/>Target site: " + exception.TargetSite);
                    HttpContext.Current.Response.Write("<br/>Stack trace: " + exception.StackTrace);
                }
            }
        }
    }
}